import heapq
import pickle
from collections import namedtuple
import time
import model
from model import ThrottleAction, SwitchAction, TurboAction

#import analysis


class Solver(object):
    GRANULARITY_SPEED = 0.1
    GRANULARITY_DIST = 3
    GRANULARITY_THROTTLE = 0.1
    THROTTLES = [i*GRANULARITY_THROTTLE for i in range(11)]

    def __init__(self, model):
        self.model = model

    def effective_dist(self, s0, s1):
        #XXX this assumes s0 and s1 are consequent states
        p0 = self.model.get_path(s0)
        p1 = self.model.get_path(s1)
        if p0 is p1:
            d = s1.dist - s0.dist
            ed = p0.effective_ratio * d
        else:
            d0 = p0.length - s0.dist
            d1 = s1.dist
            ed = p0.effective_ratio * d0 + p1.effective_ratio * d1
        return ed


    def discretize(self, state):
        return (state.lap, state.piece_index, state.from_lane, state.to_lane,
               int(state.dist / self.GRANULARITY_DIST), int(state.speed / self.GRANULARITY_SPEED),
               int(state.slip_angle / 5), int(state.slip_angle_speed / 1),
               int(state.throttle / self.GRANULARITY_THROTTLE), state.target_lane)

    def solve_laps(self):
        s0 = self.model.initState()
        sol = {}
        for lap in range(3):
            goal_s, lapsol = self.solve(s0)

            for ds, t in lapsol.items():
                if ds in sol and sol[ds] != t:
                    print 'Warning: %s old t = %f, new t = %f' % (ds, sol[ds], t)
                else:
                    sol[ds] = t

            s0 = goal_s._replace(lap=0)
        return sol

    def is_goal(self, s):
        return s.lap >= 3

    def solve_offline(self):
        #self.sol = self.solve_laps()
        goal_s, self.sol = self.solve()
        pickle.dump(self, open('sol.pickle', 'wb'))

    def _control_nearest(self, ds):
        WEIGHTS = {4: 1, 5: 5, 6: 1, 7: 0, 8: 10}
        inf = float('inf')
        def dist(d1, d2):
            d = 0
            for i, (a, b) in enumerate(zip(d1, d2)):
                if i in WEIGHTS:
                    d += abs(a-b) * WEIGHTS[i]
                elif a != b:
                    d += inf
            return d

        def show_possible():
            for k in self.sol:
                if k[:2] == ds[:2]:
                    print k

        v, k = min([(dist(a, ds), a) for a in self.sol])
        if v < inf:
            print v, ds, k
            return self.sol[k]
        else:
            show_possible()
            raise ValueError(ds)


    def control(self, state):
        ds = self.discretize(state)
        if ds in self.sol:
            a = self.sol[ds]
        else:
            print "Warning: trying to find closet control for", state
            a = self._control_nearest(ds)
        return a

class DiscreteBacktracker(dict):
    def build_solution(self, ds):
        sol = {}
        while True:
            prev_ds, prev_s, t = self[ds]
            if prev_ds is None:
                break
            sol[prev_ds] = t
            ds = prev_ds
        return sol

class DfsSolver(Solver):
    def solve(self, initState=None):
        model = self.model
        backtrack = DiscreteBacktracker() # ds -> prev_ds, throttle
        q = []
        s0 = initState or model.initState()
        ds0 = self.discretize(s0)
        q.append((ds0, s0))
        backtrack[ds0] = (None, None)

        def searched(ds):
            return (ds in backtrack)


        while q:
            ds, s = q.pop()
            print s

            if self.is_goal(s):
                sol = backtrack.build_solution(ds)
                return s, sol

            for t in self.THROTTLES:
                ns = model.next(s, t)
                if not model.is_valid(ns):
                    continue
                dns = self.discretize(ns)
                if searched(dns):
                    continue
                backtrack[dns] = (ds, t)
                q.append((dns, ns))

AstarItem = namedtuple('AstarItem', 'h tick dist s ds')
MAX_SPEED = 10.0

class ShortestPathSolver(Solver):
    Backtracker = DiscreteBacktracker
    MACRO_LEN = 30

    def solve(self, initState=None, laps=3, time_limit=None, verbose=True):
        m = self.model
        backtrack = self.Backtracker() # ds -> prev_ds, throttle
        q = []
        acts = m.get_actions(actlen=self.MACRO_LEN)
        print acts

        s0 = initState or m.initState()
        ds0 = self.discretize(s0)
        heapq.heappush(q, AstarItem(0, 0, 0, s0, ds0))
        backtrack[ds0] = (None, None, None)

        def searched(ds):
            return (ds in backtrack)

        def is_goal(s):
            return s.lap >= laps

        curr_best = 0

        start_time = time.time()

        while q:
            h, tick, dist, s, ds = heapq.heappop(q)

            if -dist > curr_best:
                curr_best = -dist
                if verbose:
                    print curr_best, tick,  s

            if is_goal(s) or (time_limit and time.time() - start_time > time_limit):
                sol = backtrack.build_solution(ds)
                return s, sol

            ntick = tick + 1
            for a in acts:
                if not m.is_action_valid(s, a):
                    continue
                ns = m.next(s, a)
                if not m.is_valid(ns):
                    continue
                dns = self.discretize(ns)
                if searched(dns):
                    continue
                #if ns.speed < 6 and ns.throttle < 0.5:
                    #continue

                backtrack[dns] = (ds, s, a)

                nd = dist - self.effective_dist(s, ns)
                nh = ntick + nd / MAX_SPEED
                nitem = AstarItem(nh, ntick, nd, ns, dns)
                heapq.heappush(q, nitem)

    def solve_online(self, s, time_limit=0.015):
        ret = self.solve(s, time_limit, verbose=False)
        if ret is None:
            return model.ThrottleAction(0.0)
        else:
            _, sol = ret
        ds = self.discretize(s)
        return sol[ds]

class LandmarkBacktracker(dict):
    def build_solution(self, ds):
        lms = []
        while True:
            prev_ds, prev_s, t = self[ds]
            if prev_ds is None:
                break
            lms.append((prev_s, t))
            ds = prev_ds
        return lms

class LandmarkSolver(ShortestPathSolver):
    Backtracker = LandmarkBacktracker

    def solve_landmark(self):
        _, lm = self.solve(laps=1)
        self.landmarks = lm

    def next_landmark(self, s):
        best_lm = None
        best_dist = float('inf')
        for s1, a in self.landmarks:
            if s1.piece_index == s.piece_index and s1.from_lane == s.from_lane \
                    and s1.to_lane == s.to_lane and s1.dist >= s.dist:
                d = self.effective_dist(s, s1)
                if d < best_dist:
                    best_dist = d
                    best_lm = (s1, a)
            elif s1.piece_index == s.piece_index+1 and s1.from_lane == s.to_lane:
                d = self.effective_dist(s, s1)
                if d < best_dist:
                    best_dist = d
                    best_lm = (s1, a)
        return best_lm

    def control(self, state):
        lm = self.next_landmark(state)
        return lm[1]

    def solve_offline(self):
        self.solve_landmark()
        pickle.dump(self, open('sol.pickle', 'wb'))

class RoutePlanner(object):
    def __init__(self, model):
        self.model = model
        self.pieces = model.pieces
        self._min_dist = {}
        self.calc_longest_straights_from_end()

    def calc_longest_straights_from_end(self):
        s = set()
        longest = 0
        curr = 0

        # take starting pieces into considieration
        for p in self.pieces:
            if isinstance(p, model.StraightPiece):
                curr += p.length
            else:
                break

        for i in range(len(self.pieces)-1, -1, -1):
            p = self.pieces[i]
            if isinstance(p, model.StraightPiece):
                curr += p.length
            else:
                curr = 0
            if curr > longest:
                longest = curr
                s.add(i)
        self.longest_straights_from_end = s
        return


    def min_dist(self, pid, from_lane):
        k = (pid, from_lane)
        if k in self._min_dist:
            return self._min_dist[k]
        if pid >= len(self.pieces):
            return 0
        piece = self.pieces[pid]
        mind = float('inf')
        for (f, t) in piece.paths:
            if from_lane == f:
                d = piece.paths[(f,t)].length + self.min_dist(pid+1, t)
                mind = min(mind, d)
        self._min_dist[k] = mind
        return mind

    def _best_lane(self, pid, from_lane, blocked_lanes):
        piece = self.pieces[pid]
        best_lane = None
        mind = float('inf')
        for (f, t) in piece.paths:
            if from_lane == f and t not in blocked_lanes:
                d = piece.paths[(f,t)].length + self.min_dist(pid+1, t)
                if d < mind:
                    mind = d
                    best_lane = t
        return best_lane

    def best_lane(self, pid, lane, blocked_lanes=[]):
        npid = (pid+1) % len(self.pieces)
        return self._best_lane(npid, lane, blocked_lanes)

class RolloutSolver(Solver):
    DEFAULT_TURBO_THROTTLE = 0.9
    def __init__(self, model):
        Solver.__init__(self, model)
        self.routePlanner = RoutePlanner(model)
        self.last_turbo = self.DEFAULT_TURBO_THROTTLE

    def switch_act(self, s0):
        bestlane = self.routePlanner.best_lane(s0.piece_index, s0.to_lane)
        if bestlane != s0.target_lane:
            act = SwitchAction(bestlane)
        else:
            act = None

        piece_index = s0.piece_index
        piece_index += 1
        if piece_index >= len(self.model.pieces):
            piece_index = 0

        #if self.model.pieces[piece_index].switch:
        for c in s0.other_cars:
            if (c.piece_index == s0.piece_index and c.dist > s0.dist and \
                   c.from_lane == s0.from_lane and  c.to_lane == s0.to_lane) or \
                    (c.piece_index == (s0.piece_index+1)%len(self.model.pieces) and \
                    c.from_lane == s0.to_lane and c.to_lane == s0.to_lane):
                dist = self.effective_dist(s0, c)
                print 'dist to front car = ', dist
                if s0.turbo_avail and dist > 60 and self.turbo_act(s0, True):
                    act = TurboAction(True)
                elif self.model.pieces[piece_index].switch:
                    lane = self.routePlanner.best_lane(s0.piece_index, s0.to_lane, [s0.to_lane]) or bestlane
                    if lane != s0.target_lane:
                        print "overtaking!!"
                        act = SwitchAction(lane)

        return act

    def rollout(self, s0, num_acc, t):
        ANGLE_LIMIT = 57
        UNCERTAINTY_PER_STEP = 1.006

        def actions():
            for _ in range(num_acc):
                yield ThrottleAction(t, 1)
            while True:
                yield ThrottleAction(0.0, 1)

        s = s0
        uncertainty = 1
        max_angle = 0
        max_angle_state = None
        for a in actions():
            s = self.model.next(s, a)
            uncertainty *= UNCERTAINTY_PER_STEP

            if s.slip_angle > max_angle:
                max_angle = s.slip_angle
                max_angle_state = s
            if abs(s.slip_angle) * uncertainty > ANGLE_LIMIT:
                return False
            if s.lap > s0.lap+1:
                print s.lap, s0.lap
                return True
            if s.speed <=4 and abs(s.slip_angle) < 10 and s.turbo_ticks <= 0:
                #print 'rollout success:', t, s0, s, max_angle
                #print 'max angle:', max_angle, max_angle_state
                return True

    def turbo_act(self, s0, for_overtake=False):
        FACTOR = 0.95
        OVERTAKE_FACTOR = 0.6
        if s0.turbo_avail:
            if s0.piece_index in self.routePlanner.longest_straights_from_end:
                return TurboAction(True)

            s = self.model.next(s0, TurboAction(True))
            tspeed = self.last_turbo

            if for_overtake:
                ts = [tspeed * OVERTAKE_FACTOR]
            else:
                ts = [1.0, (1.0+tspeed)/2, tspeed]

            a = self.throttle_act(s, ts, s.turbo_ticks)
            if a.throttle >= tspeed:
                #self.last_turbo = a.throttle
                return TurboAction(True)

    def throttle_act(self, s, TS, N):
        for t in TS:
            r = self.rollout(s, N, t)
            if r:
                break
        else:
            t = 0.0

        return ThrottleAction(t, 1)

    def solve_online(self, s, time_limit=0.01):
        N = 3
        TS = [1.0, 0.9, 0.8, 0.7, 0.6, 0.5, 0.3]
        a = self.turbo_act(s) or self.switch_act(s) or self.throttle_act(s, TS, N)

        if self.model.is_turbo_enabled(s) and isinstance(a, ThrottleAction):
            self.last_turbo = max(self.last_turbo, a.throttle)

        return a


def get_online_solver(pieces):
    m = model.Model(pieces)
    #solver = ShortestPathSolver(m)
    solver = RolloutSolver(m)
    return solver
