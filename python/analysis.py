try:
    import numpypy
except:
    pass
import numpy as np
import json
import sys
import math
from model import piece_maker, BendPiece
import fit_slip2

import argh
import glob


class Analyzer(object):
    def on_game_init(self, data):
        self.pieces = map(piece_maker(data['race']['track']['lanes']), data['race']['track']['pieces'])
        self.accumulated_piece_len = 0
        self.curr_piece = self.pieces[0]
        self.curr_dist = 0
        self.curr_speed = 0
        self.curr_throttle = 0
        self.curr_slip_angle = 0
        self.curr_slip_speed = 0
        self.curr_lateral_velocity = 0
        self.curr_path = (0, 0)
        self.crashed = False
        print 'game init'
        print data

        self.points = []
        self.slip_points = []

    def on_car_positions(self, data):
        assert len(data) == 1
        data = data[0]
        #print data
        pos = data['piecePosition']
        pi = pos['pieceIndex']
        in_dist = pos['inPieceDistance']
        slip_angle = data['angle']
        from_lane = pos['lane']['startLaneIndex']
        to_lane = pos['lane']['endLaneIndex']
        piece = self.pieces[pi]
        if piece is not self.curr_piece:
            #print pi, self.curr_piece.
            self.accumulated_piece_len += self.curr_piece.paths[self.curr_path].length
            self.curr_piece = piece

        #print dist
        dist = self.accumulated_piece_len + in_dist
        speed =  dist - self.curr_dist
        acc = speed - self.curr_speed
        slip_speed = slip_angle - self.curr_slip_angle
        slip_acc = slip_speed - self.curr_slip_speed
        tan_slip_ang = math.tan(slip_angle) - math.tan(self.curr_slip_angle)
        lateral_velocity = math.tan(slip_angle) * speed
        lateral_acc = lateral_velocity - self.curr_lateral_velocity

        #print 'pi=', pi, 'piece = %s , dist = %f' %(piece, in_dist)
        #print 'speed = ', speed
        #print 'acc = ', acc

        point = (self.curr_throttle, speed, acc, slip_angle, slip_acc)
        self.points.append(point)

        if isinstance(piece, BendPiece):
            path = piece.paths[(from_lane, to_lane)]
            piece_r = 1.0 / path.radius
            if path.angle < 0:
                piece_r = -piece_r

        else:
            piece_r = 0.0
        #slip_point = (slip_angle, piece_r, speed, slip_speed, slip_acc, from_lane, to_lane, acc, pi)
        slip_point = (self.curr_slip_angle, piece_r, self.curr_speed, self.curr_slip_speed, slip_acc, from_lane, to_lane, acc, pi)
        if not self.crashed:
            self.slip_points.append(slip_point)

        self.curr_path = (from_lane, to_lane)
        self.curr_dist = dist
        self.curr_speed = speed
        self.curr_slip_angle = slip_angle
        self.curr_slip_speed = slip_speed
        self.curr_lateral_velocity = lateral_velocity

    def show_pieces(self):
        for i, p in enumerate(self.pieces):
            print i, p


    def on_throttle(self, data):
        self.curr_throttle = data
        #print 'throttle = ', self.curr_throttle
        #pass

    def on_crash(self, data):
        self.crashed = True

    def on_spawn(self, data):
        self.crashed = False

    def load(self, fn):
        dispatch = {
                'gameInit': self.on_game_init,
                'carPositions': self.on_car_positions,
                'throttle': self.on_throttle,
                'crash': self.on_crash,
                'spawn': self.on_spawn,
                }
        for line in open(fn):
            if line[:5] not in ('RECV ', 'SEND '):
                continue
            direction = line[:4]
            try:
                msg = json.loads(line[5:])
            except ValueError:
                continue
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in dispatch:
                dispatch[msg_type](data)
            #print msg_type, data

    def plot(self):
        import matplotlib.pyplot as plt
        speeds = [p[1] for p in self.points]
        accs = [p[2] for p in self.points]
        angles = [p[3] for p in self.points]
        lateral_acc = [p[4] for p in self.points]
        #speeds = 1 / np.array(speeds)
        #angles = np.tan(angles)
        #plt.scatter(speeds, accs)
        plt.plot(angles, (lateral_acc))
        #plt.plot(accs)
        #ax2 = plt.twinx()
        #ax2.plot(ang_acc, color='r')
        plt.show()

        #notes
        # tan of angle is linear to speed

    def plot_model(self):
        import matplotlib.pyplot as plt
        x = np.array(self.slip_points)
        speed = x[:, 2]
        piece_r = x[:, 1]
        angles = x[:, 0] / 180 * np.pi
        slip_speed = x[:, 3] / 180 * np.pi
        slip_acc = x[:, 4] / 180 * np.pi
        from_lane = x[:, 5]
        to_lane = x[:, 6]
        acc = x[:, 7]
        piece_id = x[:, 8]

        #sel = np.all([np.abs(np.square(speed) * piece_r) < 0.32, np.abs(slip_speed) < 0.2], axis=0)
        #sel = np.any([np.abs(np.square(speed) * piece_r) > 0.32, np.abs(slip_speed) > 0.2], axis=0)
        sel = np.any([np.abs(np.square(speed) * piece_r) > 0.64, np.abs(slip_speed) > 0.2], axis=0)
        #sel = slice(0, len(speed))  # (piece_r != 0)
        v0 = speed[sel]
        a0  = acc[sel]
        ang = angles[sel]
        vang = slip_speed[sel]
        r = piece_r[sel]
        target = slip_acc[sel]

        w = v0 * r
        one = np.ones_like(v0)

        terms = fit_slip2.build_terms(ang, vang, v0, w, r, a0)

        clf = fit_slip2.load_model()
        yp = clf.predict(terms)

        plt.plot(target)
        plt.plot(yp, 'r-')
        plt.twinx().plot(v0*v0*r, 'g--')
        plt.show()


TARGET_PIECE = 29
class VisAnalyzer(object):
    def __init__(self):
        self.xs = []
        self.ys = []
        self.dists = []
        self.prev_dist = 0

    def on_car_positions(self, data):
        d = data[0]
        piece = d['piecePosition']
        piece_id = piece['pieceIndex']
        piece_dist = piece['inPieceDistance']
        coord = d['coordinates']
        x = coord['x']
        y = coord['y']

        print piece_id, piece_dist - self.prev_dist
        self.prev_dist = piece_dist

        lane_start = piece['lane']['endLaneIndex']
        lane_end = piece['lane']['startLaneIndex']

        lap = piece['lap']

        if piece_id == TARGET_PIECE and lap==2: # and lane_start == 0 and lane_end == 1 and lap==2:
            self.xs.append(x)
            self.ys.append(y)
            self.dists.append(piece_dist)
        return

    def load(self, fn):
        dispatch = {
                'fullCarPositions': self.on_car_positions,
                }
        j = json.load(open(fn))
        for msg in j:
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in dispatch:
                dispatch[msg_type](data)
            #print msg_type, data
    
    def plot(self):
        xys = zip(self.xs, self.ys)
        cs = range(len(xys))

        xys = np.array(xys)

        #print len(xys)
        #print xys[3], xys[len(xys)/2+2], xys[len(xys)-3]
        p = np.polyfit(cs, xys, 2)
        print p

        px = np.poly1d(p[:, 0])
        py = np.poly1d(p[:, 1])
        pxs = px(cs)
        pys = py(cs)

        def quadratic_dist(i, j):
            dist = 0
            d = 0.01
            while i<=j:
                p0 = np.array([px(i), py(i)])
                p1 = np.array([px(i+d), py(i+d)])
                dist += np.linalg.norm(p1-p0)
                i += d
            return dist

        for i in range(len(xys)-1):
            print np.linalg.norm(xys[i+1] - xys[i]), quadratic_dist(i, i+1), self.dists[i+1] - self.dists[i]

        #plt.scatter(self.dists, self.ys)
        plt.plot(pxs, pys, 'ro-')
        plt.scatter(self.xs, self.ys)
        plt.show()
        



def circumcenter( a, b, c ):
    "http://clicketysnippet.blogspot.sg/2008/10/circumcentre-of-three-points.html"
    ax, ay = a
    bx, by = b
    cx, cy = c
    
    cA = ax - cx
    cB = ax + cx
    cC = bx - cx
    cD = bx + cx
    
    cE = ay - cy
    cF = ay + cy
    cG = by - cy
    cH = by + cy
    
    cI = cA*cB
    cJ = cC*cD
    cK = cE*cF
    cL = cG*cH
    
    cM = ( cI + cK ) / 2
    cN = ( cJ + cL ) / 2
    
    d = cA * cG - cC * cE
    
    # if they're co-linear, just return a null
    if d == 0: 
        return ( None, None )
    
    center_x = ( ( cM * cG ) - ( cN * cE ) ) / d
    center_y = ( ( cN * cA ) - ( cM * cC ) ) / d
    
    return ( center_x, center_y )


def gather():
    fns = ['motion_trace.log' ]
    #fns = ['trace_keimola_v65.log', ]
    #fns = ['trace_keimola_v60.log', 'trace_keimola_v63.log', 'trace_keimola_v65.log', 'trace_keimola_v615.log', 'trace_keimola_v655.log',
            #'trace_keimola_1.log', 'trace_keimola_762.log', 'trace_keimola_crash_1.log', ]
            #'trace_keimola_crash_2.log',
            #'trace_keimola_crash_3.log',
            #'trace_keimola_crash_4.log',
            #'trace_keimola_crash_5.log',
            #'trace_keimola_crash_6.log',
            #'trace_keimola_crash_7.log',
            #'trace_keimola_crash_8.log',
            #] + glob.glob('trace_learn_fail_*.log') + glob.glob('logs/*.log')

    ps = []
    for f in fns:
        try:
            a = Analyzer()
            a.load(f)
            ps += a.slip_points
        except Exception:
            print 'failed to load', f
    np.savetxt('slip_points.txt', ps)

def plot(fn):
    a = Analyzer()
    ##a = VisAnalyzer()
    a.load(fn)
    #a.show_pieces()
    a.plot_model()
    ##a.plot()
    #np.savetxt('slip_points.txt', a.slip_points)

def showpiece(fn):
    a = Analyzer()
    a.load(fn)
    a.show_pieces()

if __name__=='__main__':
    argh.dispatch_commands([gather, plot, showpiece])

