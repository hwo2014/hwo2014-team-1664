import model

class Car(object):
    def __init__(self, name, pieces):
        self.pieces = pieces
        self.name = name
        self.state = None
        self.crashed = False
        self.accumulated_piece_len = 0
        self.curr_piece = self.pieces[0]
        self.curr_dist = 0
        self.curr_speed = 0
        self.curr_slip_angle = 0
        self.curr_throttle = 0
        self.curr_target_lane = 0
        self.curr_path = (0, 0)
        self.curr_tick = 0
        self.turbo_avail = None
        self.turbo_ticks =0
        self.turbo_factor = 1.0
        self.curr_in_piece_dist = 0.0

    def update_state(self, data, is_initial_state=False):
        pos = data['piecePosition']
        pi = pos['pieceIndex']
        in_piece_dist = pos['inPieceDistance']
        lap = pos['lap']
        piece = self.pieces[pi]

        slip_angle = data['angle']
        slip_angle_speed = slip_angle - self.curr_slip_angle


        from_lane = pos['lane']['startLaneIndex']
        to_lane = pos['lane']['endLaneIndex']

        if piece is not self.curr_piece:
            self.accumulated_piece_len += self.curr_piece.paths[self.curr_path].length
            self.curr_piece = piece
            #print 'piece = ', pi, 'speed =', self.curr_speed

        #print dist
        dist = self.accumulated_piece_len + in_piece_dist
        speed =  dist - self.curr_dist
        acc = speed - self.curr_speed

        if is_initial_state:
            self.curr_target_lane = to_lane

        #print 'dist = ', dist
        #print 'speed = ', speed
        #print 'acc = ', acc
        self.curr_in_piece_dist = in_piece_dist
        self.curr_path = (from_lane, to_lane)
        self.curr_dist = dist
        self.curr_speed = speed
        self.curr_slip_angle = slip_angle
        if self.turbo_ticks > 0:
            self.turbo_ticks -= 1

        state = model.State(lap, pi, from_lane, to_lane, in_piece_dist, speed, slip_angle, slip_angle_speed, self.curr_throttle, self.curr_target_lane, self.turbo_factor, self.turbo_ticks, self.turbo_avail, [])

        #if not self.crashed:
        #    self.model.update_fit(state)
        self.state = state
        if self.turbo_ticks:
            print 'curr', state
