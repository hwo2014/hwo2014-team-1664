import main
import socket
import sys

HOST = 'testserver.helloworldopen.com'
#HOST = 'webber.helloworldopen.com'
#HOST = 'prost.helloworldopen.com'
PORT = 8091
BOTNAME = 'motion'
BOTKEY = 'Cvl+PFH6JWzmIg'

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage: ./run track")
    else:
        joinRace = False
        track = sys.argv[1]
        if len(sys.argv) == 3:
            botname = sys.argv[2]
            joinRace = True
        else:
            botname = BOTNAME
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(HOST, PORT, botname, BOTKEY))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((HOST, int(PORT)))
        bot = main.BotInUse(s, botname, BOTKEY)
        #bot.run()
        if joinRace:
            bot.join_race(track)
        else:
            bot.create_race(track)

        bot.msg_loop()
