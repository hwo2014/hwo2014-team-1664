from car import Car

class History(object):
    def __init__(self):
        self.history = []
        self.lap_index = {0: 0}
        self.last_lap = -1

    def append_car(self, car):
        if not car.crashed:
            if not car.state.lap in self.lap_index:
                self.lap_index[car.state.lap] = len(self.history)
                self.last_lap = car.state.lap - 1

            self.history.append(car.state)

    def from_nearest(self, car):
        start = self.nearest_point_last_lap(car)
        i = start
        while i < len(self.history):
            yield self.history[i]
            i += 1

    def nearest_point_lap(self, car, lap):
        if self.last_lap != -1:
            curr_state = car.state
            hist_states = [h for h in self.history[lap] if \
                           h.state.piece_index == curr_state.piece_index]

            min_dist = float('inf')
            min_index = None
            for i,h in enumerate(hist_states):
                h_state = h.state
                delta = abs(h_state.dist - curr_state.dist)
                if delta < mind:
                    min_index = i
                    min_dist = delta

        return min_index

    def nearest_point_last_lap(self, car):
        return self.nearest_point_lap(car, self.last_lap)


class HistoryManager(object):
    def __init__(self, car_names):
        self.car_histories = {car_name: History() for car_name in car_names}

    def append_cars(self, cars):
        for c in cars:
            self.car_histories[c.name].append_car(c)

    def next_states(self, cars):
        car_replays = [self.car_histories[c.name].from_nearest(c) for c in cars]
        next_cars = []
        for cr in car_replays:
            next_car = cr.next()
            if next_car:
                next_cars.append(nc)

        while next_cars:
            yield next_cars
            next_cars = []
            for cr in car_replays:
                next_car = cr.next()
                if next_car:
                    next_cars.append(nc)
