import pprint
import json
import socket
import math
import sys
import model
from model import piece_maker, make_car, ThrottleAction, SwitchAction
#from history import *
from car import Car
import solver
import fit


class Bot(object):
    LANES = {-1: 'Left', 1: 'Right'}

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.trace = open(name + '_trace.log', 'w')
        self.first_state_update = True

        self.last_state = None

    def msg(self, msg_type, data):
        d = {"msgType": msg_type, "data": data}
        if hasattr(self, 'curr_tick'):
            d['gameTick'] = self.curr_tick
        self.send(json.dumps(d))

    def create_race(self, track, carCount=1):
        data = { "botId": {
            "name": self.name,
            "key": self.key},
            "trackName": track,
            "password": "hehe",
            "carCount": carCount
            }
        self.msg('createRace', data)

    def send(self, msg):
        self.trace.write('SEND %s\n' % msg)
        self.socket.send(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def switch_lane(self, lane_id):
        if lane_id not in self.LANES:
            print 'ERROR switch_lane:', lane_id
            self.ping()
        else:
            lane = self.LANES[lane_id]
            self.msg('switchLane', lane)

    def turbo(self):
        if not self.my_car.crashed:
            self.msg('turbo', 'pow pow pow pow boom!')

    def join_race(self,track,password=""):
        self.msg('joinRace', {"botId": {"name": self.name, "key": self.key},
                              "trackName":track,
                              "password":password,
                              "carCount": 3})

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        self.ping()

    def on_game_init(self, data):
        self.pieces = map(piece_maker(data['race']['track']['lanes']), data['race']['track']['pieces'])
        self.cars = {c['id']['name']: Car(c['id']['name'], self.pieces) for c in data['race']['cars'] }
        self.my_car = [c for k,c in self.cars.iteritems() if k == self.name][0]
        self.other_cars = [c for k,c in self.cars.iteritems() if k != self.name]
        #self.history = HistoryManager([c.name for c in self.other_cars])
        self.curr_tick = 0
        print 'game init'
        print 'cars', [c for c in self.cars.keys()]
        print data

    def on_game_start(self, data):
        self.first_state_update = True
        print("Race started")
        self.ping()

    def update_state(self, data):
        for d in data:
            c = self.cars[d['id']['name']]
            c.update_state(d, self.first_state_update)

        self.first_state_update = False

        self.my_car.state.other_cars.extend([v.state for k,v in self.cars.iteritems() if k != self.name])

        my_state = self.my_car.state
        if self.last_state is None or my_state.piece_index != self.last_state.piece_index:
            print 'piece = ', my_state.piece_index, my_state
            self.last_state = my_state

        return my_state

    def on_crash(self, data):
        print 'crash', data
        crashed_car = self.cars[data['name']]
        crashed_car.crashed = True
        if data['name'] == self.name:
            print("Crashed!!!", self.my_car.state)

    def on_spawn(self, data):
        print 'spawn', data
        spawn_car = self.cars[data['name']]
        spawn_car.crashed = False
        if data['name'] == self.name:
            self.first_state_update = True

    def on_turbo_start(self, data):
        car = self.cars[data['name']]
        car.turbo_ticks = 30 # TODO get this from data?
        if data['name'] == self.name:
            print "starting turbo"
            car.turbo_factor = car.turbo_avail["turboFactor"]
            car.turbo_ticks = car.turbo_avail["turboDurationTicks"]
            car.turbo_avail = None

    def on_turbo_end(self, data):
        car = self.cars[data['name']]
        car.turbo_ticks = 0
        car.turbo_factor = 1.0
        if data['name'] == self.name:
            print "ending turbo"

    def on_game_end(self, data):
        print("Race ended")
        pprint.pprint(data)
        print self.controller.model.fit.acc.params
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def on_turbo_avaliable(self, data):
        print("turbo available",  data)
        if not self.my_car.crashed:
            self.my_car.turbo_avail = data

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'spawn': self.on_spawn,
            'gameEnd': self.on_game_end,
            'turboAvailable': self.on_turbo_avaliable,
            'turboStart': self.on_turbo_start,
            'turboEnd': self.on_turbo_end,
            'error': self.on_error,
        }
        socket_file = self.socket.makefile()
        line = socket_file.readline()


        while line:
            self.trace.write('RECV %s\n' % line)
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if 'gameTick' in msg:
                self.curr_tick = msg['gameTick']
                #print 'got game tick', self.curr_tick
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                print("Got {0}".format(msg_type))
                #self.ping()
            line = socket_file.readline()


class NoobBot(Bot):
    def __init__(self, *args):
        Bot.__init__(self, *args)
        self.last_switch = None
        self.energy = 0
        self.steps= 0

    def on_car_positions(self, data):
        state = self.update_state(data)
        next_piece_index = (state.piece_index+1) % len(self.pieces)
        next_piece = self.pieces[next_piece_index]

        lane = data[0]['piecePosition']['lane']
        start_lane = lane['startLaneIndex']
        end_lane = lane['endLaneIndex']


        if 4 <= state.piece_index <= 7:
            e = state.speed **2
            self.energy += e
            self.steps +=1

        #print state.piece_index
        #print start_lane, end_lane
        #print self.steps, self.energy

        self.throttle(0.2)



class ControllerBot(Bot):
    def __init__(self, *args):
        Bot.__init__(self, *args)
        self.exec_countdown = 0
        self.last_act = None

    def on_game_start(self, data):
        self.model.fit.reset()
        print("Race started")
        self.ping()

    def on_car_positions(self, data):
        state = self.update_state(data)

        #test history manager
        #if state.lap >= 1:
        #    next_opponents = None
        #    nss = self.history.next_states(self.other_cars)
        #    for i in range(10):
        #        next_opponents = nss.next()

        #    print next_opponents

        if self.exec_countdown > 0:
            a = self.last_act
        else:
            a = self.control(state)
            if hasattr(a, 'length'):
                self.exec_countdown = a.length - 1
            self.last_act = a

        if isinstance(a, ThrottleAction):
            self.throttle(a.throttle)
            self.my_car.curr_throttle = a.throttle
            if self.my_car.turbo_ticks:
                self.my_car.curr_throttle *= self.my_car.turbo_factor
        elif isinstance(a, SwitchAction):
            dlane = a.lane - state.to_lane
            if dlane != 0:
                self.switch_lane(a.lane - state.to_lane)
            else:
                # TODO maybe send a throttle?
                self.ping()
            self.my_car.curr_target_lane = a.lane
        else:
            if not self.my_car.crashed:
                self.turbo()
            else:
                self.throttle(1.0)

        if not self.my_car.crashed:
            for c in self.cars.itervalues():
                if c != self.my_car:
                    if c.curr_piece == self.my_car.curr_piece and \
                    c.curr_path == self.my_car.curr_path and \
                    abs(c.curr_in_piece_dist - self.my_car.curr_in_piece_dist) < 10:
                        print "danger!!!", c.state, self.my_car.state
                        print c.curr_in_piece_dist, self.my_car.curr_in_piece_dist
            #print 'piece = ', state.piece_index
            #print 'curr', state
            #print 'act = ', a
            #print 'pred', self.controller.model.next(state, a)

        #print 'piece = ', state.piece_index
        if self.my_car.turbo_ticks:
            print 'act = ', a
            print 'pred', self.controller.model.next(state, a)
        #print

    def update_state(self, data):
        mycar_state = super(ControllerBot, self).update_state(data)
        #self.history.append_cars(self.other_cars)
        if not self.my_car.crashed:
            self.model.update_fit(mycar_state)
        return mycar_state

    def on_spawn(self, data):
        print 'spawn', data
        spawn_car = self.cars[data['name']]
        spawn_car.crashed = False
        if self.my_car == spawn_car:
            self.model.fit.reset()


class OfflineControllerBot(ControllerBot):
    def __init__(self, *args):
        ControllerBot.__init__(self, *args)
        self.controller = model.load_controller()

    def control(self, state):
        return self.controller.control(state)

class OnlineControllerBot(ControllerBot):
    def __init__(self, *args):
        ControllerBot.__init__(self, *args)

    def on_game_init(self, data):
        ControllerBot.on_game_init(self, data)
        self.controller = solver.get_online_solver(self.pieces)
        self.model = self.controller.model

    def control(self, state):
        return self.controller.solve_online(state)


#BotInUse = NoobBot
#BotInUse = OfflineControllerBot
BotInUse = OnlineControllerBot
#BotInUse = EnergyControllerBot

if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = BotInUse(s, name, key)
        bot.run()
