import itertools
import pickle
import numpy as np
import numpy.linalg as la
from numpy.polynomial.polynomial import polyvander3d, polyvander2d, polyvander
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils import array2d
from sklearn import linear_model
from sklearn.pipeline import Pipeline
from sklearn import svm
from sklearn import neighbors
from sklearn import preprocessing

class PolynomialFeatures(BaseEstimator, TransformerMixin):
    """Generate polynomial (interaction) features.

    Generate a new feature matrix consisting of all polynomial combinations
    of the features with degree less than or equal to the specified degree.
    For example, if an input sample is two dimensional and of the form
    [a, b], the degree-2 polynomial features are [1, a, b, a^2, ab, b^2].

    Parameters
    ----------
    degree : integer
        The degree of the polynomial features. Default = 2.
    include_bias : integer
        If True (default), then include a bias column, the feature in which
        all polynomial powers are zero (i.e. a column of ones - acts as an
        intercept term in a linear model).

    Examples
    --------
    >>> X = np.arange(6).reshape(3, 2)
    >>> X
    array([[0, 1],
           [2, 3],
           [4, 5]])
    >>> poly = PolynomialFeatures(2)
    >>> poly.fit_transform(X)
    array([[ 1,  0,  1,  0,  0,  1],
           [ 1,  2,  3,  4,  6,  9],
           [ 1,  4,  5, 16, 20, 25]])

    Attributes
    ----------

    `powers_`:
         powers_[i, j] is the exponent of the jth input in the ith output.

    Notes
    -----
    Be aware that the number of features in the output array scales
    exponentially in the number of features of the input array, so this
    is not suitable for higher-dimensional data.

    See :ref:`examples/plot_polynomial_regression.py
    <example_plot_polynomial_regression.py>`
    """
    def __init__(self, degree=2, include_bias=True):
        self.degree = degree
        self.include_bias = include_bias

    @staticmethod
    def _power_matrix(n_features, degree, include_bias):
        """Compute the matrix of polynomial powers"""
        # Find permutations/combinations which add to degree or less
        deg_min = 0 if include_bias else 1
        powers = itertools.product(*(range(degree + 1)
                                     for i in range(n_features)))
        powers = np.array([c for c in powers if deg_min <= sum(c) <= degree])

        # sort so that the order of the powers makes sense
        i = np.lexsort(np.vstack([powers.T, powers.sum(1)]))
        return powers[i]

    def fit(self, X, y=None):
        """
        Compute the polynomial feature combinations
        """
        n_samples, n_features = array2d(X).shape
        self.powers_ = self._power_matrix(n_features,
                                          self.degree,
                                          self.include_bias)
        return self

    def transform(self, X, y=None):
        """Transform data to polynomial features

        Parameters
        ----------
        X : array with shape [n_samples, n_features]
            The data to transform, row by row.

        Returns
        -------
        XP : np.ndarray shape [n_samples, NP]
            The matrix of features, where NP is the number of polynomial
            features generated from the combination of inputs.
        """
        X = array2d(X)
        n_samples, n_features = X.shape

        if n_features != self.powers_.shape[1]:
            raise ValueError("X shape does not match training shape")

        return (X[:, None, :] ** self.powers_).prod(-1)

def sub(x):
    y = np.zeros_like(x)
    y[1:] = x[1:] - x[:-1]
    return y

def discretize(speed, angles):
    return (int(speed/1.0), int(angles/5))

def learn_transition():
    x = np.loadtxt('slip_points.txt')
    bends = np.abs(x[:, 1]) > 0
    x = x[bends]
    speed = x[:, 2]
    angles = x[:, 0]
    dangles = sub(angles)

    t = {}
    tps = []
    for s, a, d in zip(speed, angles, dangles):
        if d > 7.0:
            continue
        k = discretize(s, a)
        if k in t:
            t[k] = max(t[k], d)
        else:
            t[k] = d
        t[k] = float(t[k])
        tps.append((float(s), float(a), float(d)))
    pickle.dump(t, open('slip_trans.pickle', 'wb'))
    pickle.dump(tps, open('slip_tuples.pickle', 'wb'))
    return t

#trans = learn_transition()


def transform_angle(a):
    #a = np.sin(a / 180 * np.pi)
    a = (a / 180 * np.pi)
    
    return a

def untransform_angle(a):
    #a = np.arcsin(a) / np.pi * 180
    a = a / np.pi * 180
    return a

def plot():
    import matplotlib.pyplot as plt
    x = np.loadtxt('slip_points.txt')

    #bends = np.abs(x[:, 1]) > 0
    bends = np.abs(x[:, 1] - 9.090909090909090468e-03) < 0.00001
    x = x[bends]
    #x = x[x[:, 1]<0]
    print x.shape

    speed6 = np.abs(x[:, 2] - 6.0) < 0.0001
    x = x[speed6]

    speed = x[:, 2]
    angles = x[:, 0] / 180 * np.pi
    cos_angles = np.cos(angles)
    sin_angles = np.sin(angles)
    dangles = angles[1:] / angles[:-1]
    ddangles = dangles[1:] / dangles[:-1]
    dddangles = ddangles[1:] - ddangles[:-1]
    ddddangles = dddangles[1:] - dddangles[:-1]
    dcos = cos_angles[1:] - cos_angles[:-1]
    plt.scatter(angles[2:], ddangles)
    #plt.scatter(dangles[:-1], ddangles)
    #plt.scatter(speed[:-2], ddangles)
    #plt.scatter(angles[:-3], dddangles)
    #plt.scatter(sin_angles[:-1], dcos)
    #plt.scatter(ddangles, sin_angles[:-2])
    #plt.scatter(ddangles, cos_angles[:-2])
    #plt.scatter(ddangles, sin_angles[:-2])
    #plt.plot(cos_angles[:-2], label='cos')
    #plt.plot(sin_angles[:-2], label='sin')
    #plt.plot(angles)

    #v = -0.05432032 * cos_angles + 0.00392498 * sin_angles
    #plt.plot(v)

    #plt.legend()
    #tx = plt.twinx()

    #ddangles[np.abs(ddangles) > 0.01] = 0
    #tx.plot(ddangles, label='ddangles', color='r')

    #dddangles[np.abs(dddangles) > 0.01] = 0
    #tx.plot(dddangles, label='dddangles', color='r')

    #ddddangles[np.abs(ddddangles) > 0.01] = 0
    #tx.plot(ddddangles, label='ddddangles', color='r')
    #tx.legend()

    #plt.scatter(angles[:-1], dangles)
    #plt.ylim((-2, 2))
    #plt.show()

    #x = np.vstack((cos_angles, sin_angles, np.ones_like(sin_angles))).transpose()
    #print sum(np.abs(ddangles))
    #print la.lstsq(x[:-2], ddangles)
    #absdang = np.abs(dangles)
    #angratio = absdang / np.abs(angles)[1:]
    #plt.hist(angratio, range=(0, 2), bins=100)
    #plt.hist(dangles)
    plt.show()
    #print max(absdang[absdang < 0.1]) / np.pi * 180


def fit():
    # (slip_angle, piece_r, speed)
    x = np.loadtxt('slip_points.txt')

    bends = np.abs(x[:, 1]) > 0
    x = x[bends]
    #x = x[x[:, 1]<0]
    print x.shape

    angles = transform_angle(x[:, 0])
    ddangles = sub(sub(angles))
    cos_angles = np.cos(angles)
    sin_angles = np.sin(angles)
    piece_r = x[:, 1]
    #print piece_r
    speed = x[:, 2]
    acc = sub(speed)

    ddangles[np.abs(ddangles) > 0.1] = 0

    target = np.zeros_like(ddangles)
    target[2:] = ddangles[2:]



    features = np.vstack((angles, cos_angles, sin_angles, piece_r, speed, acc)).transpose()

    estimators = [
        #('preprocess', preprocessing.StandardScaler()),
        ('poly', PolynomialFeatures(degree=3)),
        ('linear', linear_model.LinearRegression()),
        #('linear', linear_model.Ridge(alpha=0.5)),
        #('linear', linear_model.Lasso(alpha=0.002)),
        #('knn', neighbors.KNeighborsRegressor(n_neighbors=3, weights='distance')),
        ]
    clf = Pipeline(estimators)
    #f = poly.fit_transform(features)
    clf.fit(features, target)
    tt = clf.predict(features)
    for t0, t1 in zip(tt, target):
        print t0, t1

    print 'score = ', clf.score(features, target)
    print clf.steps[0][1].coef_
    return clf



def next(slip_angle, piece_r, speed, acc):
    if piece_r == 0:
        return slip_angle / 2
    #piece_r = -piece_r

    angles = transform_angle(slip_angle)
    #coss = np.cos(slip_angle / 180 * np.pi)
    cos_angles = np.cos(angles)
    sin_angles = np.sin(angles)

    x = np.array([[ cos_angles, sin_angles, piece_r, speed, acc]])
    y = a.predict(x)
    #return slip_angle - r[0]
    #return np.arctan(np.tan(slip_angle) + r)[0] * 180 / np.pi
    return y[0]
    #return untransform_angle(y[0])


def check():
    s = 0
    for i in range(10):
        s = next(s, -0.01, 7, 0.1)
        print s

#a = fit()
#check()
plot()
