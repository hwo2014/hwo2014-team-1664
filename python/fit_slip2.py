import numpy as np
from sklearn import linear_model
from sklearn.pipeline import Pipeline
from sklearn import tree
import matplotlib.pyplot as plt
import pickle

def build_terms(ang, vang, v0, w, r, a0):
    L = 20
    one = np.ones_like(v0)
    vx =  L * (vang + w) * np.sin(ang) + v0
    vy =  L * (vang + w) * np.cos(ang)
    vdir = np.arctan2(vy, vx)

    signs = [
            np.sign(r),
            np.sign(ang),
            np.sign(r)*np.sign(ang),
            np.sign(r)*np.sign(vang),
            #np.sign(vang),
            np.sign(vx),
            np.sign(vy),
            #(v0*v0*r) < 0.32,
            ]

    forces = [
            ang,
            vang,
            vang*vang,
            w,
            w*w,
            v0,
            v0*v0*r,
            #r,
            #r*r,
            np.sqrt(np.abs(r)),
            np.square(vang + w),
            a0,
            one,
            ]

    dirs = [
            np.sin(ang),
            np.cos(ang),
            np.sin(ang)**2,
            np.cos(ang)**2,
            np.sin(ang) * np.cos(ang),
            np.tan(ang),
            np.sin(vdir),
            np.cos(vdir),
            np.sin(vdir-ang),
            np.cos(vdir-ang),
            np.sin(vdir+ang),
            np.cos(vdir+ang),
            1,
            np.sign(r),
            ]

    terms = []
    for f in forces:
        for d in dirs:
            for s in signs:
                terms.append(f*d*s)
    terms = np.array(terms).transpose()
    return terms

#def build_terms(ang, vang, v0, w, r, a0):
    #terms = np.array([ang, vang, v0, w, a0])
    #terms = np.array(terms).transpose()
    #return terms

MODLE_NAME = {
        'slow':  'slip_linear_slow.pickle',
        'fast':  'slip_linear_fast.pickle',
        }

def train():
    x = np.loadtxt('slip_points.txt')
    x = x[20000:]
    speed = x[:, 2]
    piece_r = x[:, 1]
    angles = x[:, 0] / 180 * np.pi
    slip_speed = x[:, 3] / 180 * np.pi
    slip_acc = x[:, 4] / 180 * np.pi
    from_lane = x[:, 5]
    to_lane = x[:, 6]
    acc = x[:, 7]
    piece_id = x[:, 8]

    def offest(x, d):
        z = np.zeros_like(x)
        z[:-d] = x[d:]
        return z

    def train_seg(lower, upper):
        sel = slice(0, len(speed))  # (piece_r != 0)
        #sel = (np.abs(np.square(speed) * piece_r) > 0.32)
        vvr = np.abs(np.square(speed) * piece_r)
        sel = np.all([vvr < upper, vvr > lower], axis=0)
        #sel = (np.abs(np.square(speed) * piece_r) < 0.32)
        #sel = (piece_r != 0)
        v0 = speed[sel]
        a0  = acc[sel]
        ang = angles[sel]
        vang = slip_speed[sel]
        r = piece_r[sel]
        target = slip_acc[sel]

        w = v0 * r

        terms = build_terms(ang, vang, v0, w, r, a0)

        print terms.shape, target.shape

        #clf = linear_model.Ridge(alpha=0.1)
        #clf = linear_model.Lasso(alpha=0.000000001, max_iter=1000000)
        clf = linear_model.LinearRegression()
        #clf = tree.DecisionTreeRegressor(min_samples_split=4)
        clf.fit(terms, target)


        print lower, upper, 'score = ', clf.score(terms, target)
        return clf

    SEGS = [0.32, 0.40, 0.48, 0.56, 0.64, 0.8, np.inf]
    clfs = []
    for i in range(len(SEGS)-1):
        clf = train_seg(SEGS[i], SEGS[i+1])
        clfs.append(clf)

    c = (SEGS, clfs)

    pickle.dump(c, open('segs.pickle', 'wb'))

    #tt = clf.predict(terms)

    #plt.plot(target)
    #plt.plot(tt, 'r-')
    #plt.show()

def load_model():
    clf = pickle.load(open('slip_linear.pickle', 'rb'))
    return clf


if __name__=='__main__':
    train()
