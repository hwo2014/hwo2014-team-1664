import math
import numpy as np
import numpy.linalg as la
import scipy.optimize as opt

def fitted_seilgu(a, b, c, d):
    def f(ang, vang, v0, w, r, a0):
        absr = np.abs(r)
        r_sign = np.sign(r)
        fc = (v0*v0*absr-d) * c
        fc = np.maximum(fc, 0)
        aa = fc *r_sign  - a*ang - b*vang
        return aa
    return f

slip_func = fitted_seilgu

class Fitter(object):
    def __init__(self):     
        self.xs = []
        self.ys = []
        self.params = np.array(self.INIT_PARAM)

class SlipFitter(Fitter):
    #INIT_PARAM = [0.007875, 0.1, 0.04730611,  0.30828224] # original
    INIT_PARAM = [0.0075, 0.1, 0.05,  0.30] # conservative
    MINDATA = 50
    MAXDATA = 2000


    def to_x(self, path, state, linear_acc):
        slip_angle = state.slip_angle / 180 * math.pi
        slip_angle_speed = state.slip_angle_speed / 180 * math.pi
        piece_r = path.one_over_radius
        if hasattr(path, 'angle') and path.angle < 0:
            piece_r = -piece_r
        speed = state.speed
        w = speed * piece_r

        x = (slip_angle, slip_angle_speed, speed, w, piece_r, linear_acc)
        return x

    def add_point(self, path, state, linear_acc, slip_acc):
        if state.from_lane != state.to_lane:
            # we don't know the dynamic at switch lane..
            return
        #if abs(slip_acc) < 0.0001:
            #return
        x = self.to_x(path, state, linear_acc)
        y = slip_acc / 180 * math.pi
        self.xs.append(x)
        self.ys.append(y)

    def fit(self):
        print 'data points = ', len(self.xs)
        if self.MINDATA > len(self.xs) or self.MAXDATA < len(self.xs):
            return
        x = np.array(self.xs)
        y = np.array(self.ys)
        xs = [x[:, i] for i in range(x.shape[1])]
        fitfunc = lambda p: slip_func(*p)(*xs)
        #errfunc = lambda p: fitfunc(p) - y
        #ret, _ = opt.leastsq(errfunc, self.INIT_PARAM)
        errfunc = lambda p: np.mean(np.abs(fitfunc(p)-y))
        ret = opt.fmin_powell(errfunc, self.INIT_PARAM)
        self.params = ret
        print 'slip param = ', ret, 'err = ', np.sum(np.square(errfunc(ret)))

    def predict(self, path, state, linear_acc):
        x = self.to_x(path, state, linear_acc)
        y = slip_func(*self.params)(*x)
        y = y / math.pi * 180
        return y

class SegmentedSlipFitter(object):
    THRESHOLDS = [0.5, 0.65]
    
    def __init__(self):
        self.fitters = [SlipFitter() for _ in range(len(self.THRESHOLDS)+1)]

    def get_fitter_index(self, path, state):
        vvr = (state.speed ** 2) * path.one_over_radius
        for i, t in enumerate(self.THRESHOLDS):
            if vvr < t:
                return i
        return len(self.THRESHOLDS)

    def add_point(self, path, state, linear_acc, slip_acc):
        fi = self.get_fitter_index(path, state)
        f = self.fitters[fi]
        f.add_point(path, state, linear_acc, slip_acc)

    def fit(self):
        for f in self.fitters:
            f.fit()

    def predict(self, path, state, linear_acc):
        fi = self.get_fitter_index(path, state)
        y = self.fitters[fi].predict(path, state, linear_acc)
        for i in range(0, fi):
            yi = self.fitters[i].predict(path, state, linear_acc)
            y = max(y, yi, key=abs)
        return y

class SpeedFitter(Fitter):
    INIT_PARAM = [  -0.02, 0.2]

    def add_point(self, state, acc):
        x = (state.speed, state.throttle)
        y = acc
        self.xs.append(x)
        self.ys.append(y)

    def _fit_lstsq(self):
        x = np.array(self.xs[4:])
        y = np.array(self.ys[4:])
        ret = la.lstsq(x, y)
        effs = ret[0]
        residuals = ret[1]
        self.params = effs
        print 'speed param = ', effs, 'residuals = ', residuals

    def _fit(self):
        x = np.array(self.xs)
        y = np.array(self.ys)


    def fit(self):
        #return
        if len(self.xs) < 5:
            return
        self._fit_lstsq()

    def predict(self, state):
        x = np.array([state.speed, state.throttle])
        return np.dot(self.params,  x)


class FitterManager(object):
    def __init__(self):
        self.reset()
        self.acc = SpeedFitter()
        self.slip = SlipFitter()
        self.slip = SegmentedSlipFitter()
        #self.slip = LearnedSlip()
        self.update_count = 0

    def reset(self):
        self.last_state = None
    
    def update(self, path, state):
        if self.last_state:
            a = state.speed - self.last_state.speed
            self.acc.add_point(self.last_state, a)

            slip_acc = state.slip_angle_speed - self.last_state.slip_angle_speed
            self.slip.add_point(path, state, a, slip_acc)

            self.update_count += 1
        self.last_state = state

    def fit(self):
        if self.update_count < 20:
            self.acc.fit()

        if self.update_count % 200 == 0:
            self.slip.fit()

