try:
    import numpypy
except:
    pass
import numpy as np
import math
import pickle
from collections import namedtuple
from utils import QuadraticCurve
import fit

CarID = namedtuple('Car', 'name color')

def make_car(d):
    return Car(d['id']['name'], d['id']['color'])

class BendCrossSimple(object):
    C = 1.03183
    def __init__(self, from_lane_offset, to_lane_offset, piece):
        self.length = abs(piece.angle_rad * piece.radius * self.C)
        self.effective_ratio = piece.length(0) / self.length
        self.radius = piece.radius
        self.one_over_radius = 1.0 / self.radius
        self.angle = piece.angle


class BendLane(object):
    def __init__(self, lane_offset, piece):
        self.length = piece.length(lane_offset)
        self.effective_ratio = piece.length(0) / self.length
        self.radius = piece._radius(lane_offset)
        self.one_over_radius = 1.0 / self.radius
        self.angle = piece.angle

class StraitCross(object):
    def __init__(self, from_lane_offset, to_lane_offset, piece):
        self.length = math.sqrt((from_lane_offset-to_lane_offset)**2 + piece.length**2)
        self.effective_ratio = piece.length / self.length
        self.one_over_radius = 1.0 / 100

class StraitLane(object):
    def __init__(self, lane_offset, piece):
        self.length = piece.length
        self.effective_ratio = piece.length / self.length
        self.one_over_radius = 0

def test_BendCrossCurve():
    #piece = BendPiece({'angle': 22.5, 'radius': 200, 'switch': True})
    piece = BendPiece({'angle': -45, 'radius': 100, 'switch': True}, [-10, 10])
    #curve = BendCrossCurve(10, -10, piece)
    #print piece.length, curve.length
    print piece.paths[(1,0)].length, piece.paths[(1,1)].length, piece.paths[(1,0)].length, piece.paths[(0,1)].length


class Piece(object):
    def construct_paths(self):
        lanes = self.lanes
        paths = {}
        for i, l in enumerate(lanes):
            paths[(i, i)] = self.Lane(l, self)

        if self.switch:
            for i in range(len(lanes)-1):
                j = i+1
                l0 = lanes[i]
                l1 = lanes[j]
                paths[(i, j)] = self.Cross(l0, l1, self)
                paths[(j, i)] = self.Cross(l1, l0, self)
        return paths

class StraightPiece(Piece):
    Lane = StraitLane
    Cross = StraitCross
    def __init__(self, data, lanes):
        self.lanes = lanes
        self.length = data['length']
        if 'switch' in data:
            self.switch = data['switch']
        else:
            self.switch = False

        self.paths = self.construct_paths()

    def __str__(self):
        return '%s(length=%d, switch=%s)' % (self.__class__.__name__, self.length, self.switch)

class BendPiece(Piece):
    Lane = BendLane
    Cross = BendCrossSimple
    def __init__(self, data, lanes):
        self.lanes = lanes
        self.angle = data['angle']
        self.radius = data['radius']
        if 'switch' in data:
            self.switch = data['switch']
        else:
            self.switch = False

        self.paths = self.construct_paths()

    @property
    def angle_rad(self):
        return math.pi * self.angle / 180

    def _radius(self, offset):
        if self.angle_rad > 0:
            r = self.radius - offset
        else:
            r = self.radius + offset
        return r

    def length(self, offset):
        r = self._radius(offset)
        return abs(self.angle_rad * r)

    def __str__(self):
        return '%s(angle=%f, radius=%f, switch=%s)' % (self.__class__.__name__, self.angle, self.radius, self.switch)

def piece_maker(lanes):
    lanes = [l['distanceFromCenter'] for l in lanes]
    def make_piece(data):
        if 'angle' in data:
            return BendPiece(data, lanes)
        else:
            return StraightPiece(data, lanes)
    return make_piece


State = namedtuple('State', 'lap piece_index from_lane to_lane dist speed slip_angle slip_angle_speed throttle target_lane turbo_factor turbo_ticks turbo_avail other_cars')

SwitchAction = namedtuple('SwitchAction', 'lane')
ThrottleAction = namedtuple('ThrottleAction', 'throttle length')
TurboAction = namedtuple('TurboAction', 'on')

def discretize(speed, angles):
    return (int(speed/1.0), int(angles/5))

class Model(object):
    def __init__(self, pieces):
        self.pieces = pieces
        self.fit = fit.FitterManager()

    def get_path(self, state):
        piece = self.pieces[state.piece_index]
        path = piece.paths[(state.from_lane, state.to_lane)]
        return path

    def update_fit(self, state):
        path = self.get_path(state)
        self.fit.update(path, state)
        self.fit.fit()

    def next_slip(self, state, acc):
        path = self.get_path(state)
        acc = self.fit.slip.predict(path, state, acc)

        slip_angle_speed = state.slip_angle_speed + acc
        slip_angle = state.slip_angle + slip_angle_speed
        return slip_angle, slip_angle_speed

    def _next(self, state, act):
        piece = self.pieces[state.piece_index]

        #a = acc(throttle, state.speed)
        a = self.fit.acc.predict(state)

        lap = state.lap
        speed = state.speed + a
        dist = state.dist + speed

        piece_index = state.piece_index
        from_lane = state.from_lane
        to_lane = state.to_lane
        target_lane = state.target_lane

        path_len = piece.paths[(from_lane, to_lane)].length
        if dist > path_len:
            dist -= path_len
            piece_index += 1
            if piece_index >= len(self.pieces):
                piece_index = 0
                lap += 1

            from_lane = to_lane
            if self.pieces[piece_index].switch:
                to_lane = target_lane

        slip_angle, slip_angle_speed = self.next_slip(state, a)

        turbo_ticks = state.turbo_ticks
        turbo_factor = state.turbo_factor
        turbo_avail = state.turbo_avail


        throttle = state.throttle
        if isinstance(act, ThrottleAction):
            throttle = act.throttle
        elif isinstance(act, TurboAction):
            turbo_ticks = state.turbo_avail['turboDurationTicks']
            turbo_factor = state.turbo_avail['turboFactor']
            turbo_avail = None
        else:
            target_lane = act.lane

        if turbo_ticks > 0:
            throttle = throttle * turbo_factor
            turbo_ticks = turbo_ticks -1

        next_state = State(lap, piece_index, from_lane, to_lane, dist, speed, slip_angle, slip_angle_speed, throttle, target_lane, turbo_factor, turbo_ticks, turbo_avail, state.other_cars)
        return next_state

    def next(self, state, act):
        if isinstance(act, ThrottleAction):
            actlen = act.length
        else:
            actlen = 1
        s = state
        for _ in range(actlen):
            s = self._next(state, act)
        return s

    def is_valid(self, state):
        if abs(state.slip_angle) > 55:
            return False
        else:
            return True

        pi = state.piece_index
        piece = self.pieces[pi]
        if isinstance(piece, BendPiece):
            if pi in PER_PIECE_LIMIT:
                return state.speed < PER_PIECE_LIMIT[pi]
            else:
                return state.speed < BEND_SPEED_LIMIT
        else:
            return True

    def is_turbo_enabled(self, state):
        return state.turbo_ticks > 0

    def is_action_valid(self, state, act):
        path = self.get_path(state)
        if isinstance(act, SwitchAction) and (act.lane == state.to_lane or state.dist + state.speed*3 > path.length):
            return False
        return True

    def get_actions(self, granularity=0.1, actlen=1):
        #TS = [0.0, 0.5, 0.6, 0.7, 1.0]
        TS = [0.0, 0.5,  1.0]
        #TS = [0.0,  1.0]
        throttles = [ThrottleAction(t, actlen) for t in TS]
        switchs = [SwitchAction(0), SwitchAction(1)]
        return throttles + switchs

    def initState(self):
        return State(0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
                1.0, 0, None, [])

def load_controller():
    c = pickle.load(open('sol.pickle', 'rb'))
    return c


if __name__=='__main__':
    #solve()
    test_BendCrossCurve()
