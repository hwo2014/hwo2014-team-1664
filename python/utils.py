import numpy as np

class QuadraticCurve(object):
    "http://en.wikipedia.org/wiki/B%C3%A9zier_curve#Quadratic_B.C3.A9zier_curves"
    D = 0.1
    def __init__(self, start, end, control):
        self.start = start
        self.end = end
        self.control = control

    def __call__(self, t):
        p0, p1, p2 = self.start, self.control, self.end
        a = (1-t)**2
        b = 2*t*(1-t)
        c = t**2
        p = a*p0 + b*p1 + c*p2
        return p

    def length(self):
        i = 0.0
        dist = 0
        while i<=1:
            p0 = self(i)
            p1 = self(i+self.D)
            dist += np.linalg.norm(p1-p0)
            i += self.D
        return dist




def test_quadratic():
    q = QuadraticCurve(np.array([0, 0]), np.array([1,1]), np.array([0.5, 0.5]))
    print q.length()

if __name__=='__main__':
    test_quadratic()
        
