import analysis
import model
import solver

a = analysis.Analyzer()
a.load('logs/trace_keimola_1.log')
pieces = a.pieces
m = model.Model(pieces)
#solver = solver.ShortestPathSolver(m)
solver = solver.LandmarkSolver(m)
solver.solve_offline()


