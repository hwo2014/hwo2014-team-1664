#!/bin/sh
mkdir -p logs/
python testbot.py germany
fn=learn_germany_$(date +"%s").log
mv trace.log logs/$fn

python testbot.py france
fn=learn_france_$(date +"%s").log
mv trace.log logs/$fn

python testbot.py keimola
fn=learn_keimola_$(date +"%s").log
mv trace.log logs/$fn

python analysis.py gather
python fit_slip2.py

