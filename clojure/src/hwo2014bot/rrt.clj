(ns hwo2014bot.rrt
  (:require [hwo2014bot.model :refer :all]
            [loom.graph :refer :all])
  )

(defprotocol RRTModel
  (sample-free [this])
  (nearest [this g x-rand])
  (steer [this x-nearest x-rand])
  (obstracle-free [this x-nearest x-new])
  (near [this g x-new distance])
  (line-cost [this x-near x-new])
  )


;; IMPORTANT: distance is not symmetric
(defn config-l2 [exp-cum-dist src tgt]
  (if (or (> (:pc src) (:pc tgt))
          (and (<= (:pc src) (:pc tgt))
               (> (:in-pc-dist src)
                  (:in-pc-dist src))))
    Float/MAX_VALUE
    (let [fns [:speed
               :in-pc-dist
               :lane-to
               :lane-from
               (fn [c] (exp-cum-dist (:pc c)))
               :angle]]
      (map (map #(% src) fns) (map #(% tgt) fns))
      ))
  )

;; -(angles[k]/10+dangles[k])*0.8*np.exp(-0.08*t)

(defrecord RaceRRTModel
    [pieces
     lane-offsets
     full-model
     exp-cum-dist
     ]
  RRTModel
  (sample-free [this]
    (let [lane-to (rand-int (count lane-offsets))
          lane-from (filter #(and (>= % 0) (< % (count lane-offsets)))
                            (map #(+ lane-to %)[-1 0 1]))
          pc (rand-int (count pieces))]
      {:speed (rand 12.0)
       :in-pc-dist (rand (distance (pieces pc) lane-to lane-from))
       :pc pc
       :lane-to lane-to
       :lane-from lane-from
       :angle 0})
    )

  (nearest [this g x-rand]
    (apply min-key #(config-l2 exp-cum-dist % x-rand) (nodes g)))

  (steer [this x-nearest x-rand]
    (comment (if (= (:pc x-neareset) (:pc x-rand))))
    )
  (obstracle-free [this x-nearest x-new])
  (near [this g x-new distance])
  (line-cost [this x-near x-new])
  )

(defn make-RRT-model [track-data]
  (let [full-model (make-full-model track-data)]
    (->RaceRRTModel
     (:pieces full-model)
     (:lane-offsets (:lane-offsets (first (:pieces full-model))))
     full-model
     (:cum-dist full-model)
     ))
  )
