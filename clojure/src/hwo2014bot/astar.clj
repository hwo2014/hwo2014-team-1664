(ns hwo2014bot.astar
  (:require [hwo2014bot.model :refer :all]
            [clojure.data.priority-map :refer :all]))

(defn- discretize [num gran]
  (* gran (Math/round (float (/ num gran)))))

(defn discretize-state [state]
  (conj state
        {:speed  (discretize (:speed state) 0.2)
         :in-pc-dist (discretize (:in-pc-dist state) (max (:speed state) 0.2))
         :angle-speed (discretize (:angle-speed state) 1)
         :angle-acc  (discretize (:angle-acc state) 1)
         :angle (discretize (:angle state) 3)
         }))


(defn backtrack [pred start goal]
  (loop [s goal
         path '()]
    (if (= s start)
      path
      (do
        (println s)
        (assert (not= (pred s) s))
        (recur (pred s) (conj path s)))))
  )

;; TODO: test this
(defn neighbour-nodes [model state]
  (for [t [1.0 0 (:speed state)]
        l [1 0 -1]
        :let [ns (next-state model state {:throttle t :lane-delta 0})]
        :when (or (= l 0)
                  (and (> (:pc ns) (:pc state))
                       (switchable ((:pieces model) (:pc ns)))
                       (let [nl (+ (:lane-to state) l)]
                         (and (>= nl 0) (< nl (count (:lane-offsets model)))))))
        ]
    (discretize-state (next-state model state {:throttle t :lane-delta l}))
    )
  )


(defn h-estimate [model state]
  (/ (- (last (:cum-dist model)) ((:cum-dist model) (:pc state))) 12)
  )

(defn astar [model start num-pcs]
  (loop [closed (transient #{})
         opened (priority-map start (h-estimate model start))
         pred (transient {})
         g-score (transient {start 0})
         f-score (transient {start (h-estimate model start)})]
    (let [curr (ffirst opened)
          ]
      (cond (nil? curr)
            [] ;;failure
            (>= (:pc curr) (dec num-pcs))
            (do (println "success")
                (backtrack (persistent! pred) start curr))
            :else
            (let [[new-f-score new-g-score new-opened new-pred]
                  (loop [nn (filter #(not (closed %))
                                    (neighbour-nodes model curr))
                         nf f-score
                         ng g-score
                         np pred
                         no (dissoc opened curr)]
                    (if (empty? nn)
                      [nf ng no np]
                      (let [n (first nn)
                            tent-g-score (if (contains? n :crash)
                                           (+ (ng curr) 5000)
                                           (+ (ng curr) 1)
                                             )
                            ]
                        (if (or (not (contains? no n))
                                (< tent-g-score (ng n)))
                          (recur (rest nn)
                                 (conj! nf {n (+ tent-g-score (h-estimate model n))})
                                 (conj! ng {n tent-g-score})
                                 (conj! np {n curr})
                                 (conj no [n (+ tent-g-score (h-estimate model n))]))
                          (recur (rest nn)
                                 nf
                                 ng
                                 np
                                 no
                                 )
                          ))
                      )
                    )]
              (recur (conj! closed curr)
                     new-opened
                     new-pred
                     new-g-score
                     new-f-score))
            )
      )
    ))
