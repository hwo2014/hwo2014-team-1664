(ns hwo2014bot.speed-advisor
  (:require [hwo2014bot.model :refer :all]))


(defn shortest-path-rec [path-model start-state laps]
  (if (>= (:lap start-state) laps)
    ['() (:distance start-state)]
    (let [curr-lane (:lane start-state)
          piece ((:pieces path-model) (:pi start-state))
          acts (if (switchable piece)
                 (filterv
                  #(and (>= % 0) (< % (:num-lanes path-model)))
                  (mapv (partial + curr-lane) [1 0 -1]))
                 [curr-lane])
          ns-acts (map #(vector
                         (next-state path-model start-state %)
                         %)
                       acts)
          ]
      (apply min-key second
             (map
              (fn [[ns act]]
                (let [[act-seq dist] (shortest-path-rec path-model ns laps)]
                  [(conj act-seq (- act curr-lane)) dist])
                )
              ns-acts))
      )))

;; seek the shortest path by switching lane
(defn shortest-path [path-model start-state laps]
  (vec (first (shortest-path-rec path-model start-state laps))))

;; TODO: check for slip angle as well
(defn check-feasible [static-model curr-state target-distance target-speed]
  (loop [s curr-state]
    (cond
     (and (<= (:speed s) target-speed) )
     true
     (> (:distance s) target-distance)
     false
     :else
     (recur (next-state static-model s 0))
     )
    ))


(def SPEED_EPS 0.2)

(defn binary-search-speed [static-model curr-state target-distance target-speed]
  (loop [lo 0
         hi (:speed curr-state)]
    (if (<= (- hi lo) SPEED_EPS)
      hi
      (let [mid (/ (+ hi lo) 2)]
        (if (check-feasible static-model (assoc curr-state :speed mid)
                            target-distance
                            target-speed)
          (recur mid hi)
          (recur lo mid)
          )))))

(defn init-speed-limits [static-model
                         path-model
                         shortest-path-policy
                         start-p-state]
  (let [p-states (loop [ps start-p-state
                        pss []
                        pol shortest-path-policy]
                   (if (empty? pol)
                     pss
                     (recur (next-state path-model ps
                                        (+ (:lane ps) (first pol)))
                            (conj pss ps)
                            (rest pol))))

        pieces (:pieces path-model)

        num-pieces (count pieces)

        cum-dist (mapv :distance p-states)

        ;; manually set speed limit at the beginning of each turn
        ;; TODO: put in the correct lane number
        turn-pis (filter #(not= 0 (one-over-radius (second %) 0 0))
                         (map vector (range) pieces))

        switch-lane-pis (filter #(not= 0 (shortest-path-policy (first %)))
                                (map vector (range) pieces))

        turn-limits (mapv (fn [[i _]]
                            (let [next-pi (mod (inc i) num-pieces)]
                              [[(cum-dist i)
                                (+ (cum-dist i)
                                   (* 1/2 (- (cum-dist next-pi) (cum-dist i))))
                                ]
                               (if (> (/ 1 (one-over-radius (pieces i)
                                                            (:lane (p-states i))
                                                            (:lane (p-states next-pi))))
                                      150)
                                 8.5
                                 6.5)
                               ]
                              ))
                          turn-pis)

        switch-limits (mapv (fn [[i _]] (let [next-pi (mod (inc i) num-pieces)]
                                          [[(cum-dist i)
                                            (cum-dist next-pi)
                                            ] 6.6]))
                            switch-lane-pis)
        ]
    (vec (sort-by ffirst (into turn-limits switch-limits)))
    )
  )


;; refine a shortest path policy
;; speed-advisor assumes starting at the beginning of a piece
;; TODO: allow replanning, now assume everytime starts from beginning of lap
(defn compute-speed-limits [static-model
                            path-model
                            shortest-path-policy
                            start-p-state
                            start-distance]
  (let [init-spd-limits (init-speed-limits static-model path-model
                                           shortest-path-policy
                                           start-p-state)
        ]
    ;; check that speed limits are feasible, so that controller only
    ;; needs to follow the nearest limit
    (loop [rev-spd-limits (rest (reverse init-spd-limits))
           next-limit (last init-spd-limits)
           new-spd-limits (conj '() (last init-spd-limits))]
      ;; check if the speed limit is not consistent
      ;; if not do binary search for find new limit
      (if (empty? rev-spd-limits)
        ;; post-processing to add back the starting distance
        (map (fn [[interval speed]]
               [(mapv (partial + start-distance) interval) speed])
             new-spd-limits)
        (let [curr-limit (first rev-spd-limits)
              curr-limit-end (second (first curr-limit))
              curr-limit-speed (second curr-limit)
              state {:speed curr-limit-speed
                     :distance curr-limit-end
                     :angle 0}
              target-distance (ffirst next-limit)
              target-speed (second next-limit)
              ]
          (if (check-feasible static-model state target-distance target-speed)
            (recur (rest rev-spd-limits )
                   (first rev-spd-limits)
                   (conj new-spd-limits (first rev-spd-limits)))
            ;; do binary search to find the new limit
            (let [new-spd (binary-search-speed static-model state
                                               target-distance
                                               target-speed)
                  new-spd-limit [(first curr-limit) new-spd]]
              (recur (rest rev-spd-limits)
                     new-spd-limit
                     (conj new-spd-limits new-spd-limit)))

            )))
      )
    ))

(def MAX_SPEED 12)

(defn speed-advisor [static-model speed-limits
                     curr-speed curr-distance curr-angle turbo-len turbo-factor]
  (let [nearest-limit (some #(if (> (second (first %)) curr-distance) %) speed-limits)
        ;;_ (println nearest-limit)
        state {:speed curr-speed
               :distance curr-distance
               :angle curr-angle
               }
        acc-ns (next-state static-model state 1.0)
        dec-ns (next-state static-model state 0.0)
        turbo-ns (loop [tns state
                        ticks turbo-len]
                   (if (zero? ticks)
                     tns
                     (recur (next-state static-model tns turbo-factor) (dec ticks))))
        target-distance (ffirst nearest-limit)
        target-speed (second nearest-limit)
        ]
    (cond
     (nil? nearest-limit)
     (if (> turbo-len 0) -1 MAX_SPEED)
     (and (<= curr-distance (second (first nearest-limit)))
          (>= curr-distance (ffirst nearest-limit)))
     target-speed
     (check-feasible static-model turbo-ns target-distance target-speed)
     (if (> turbo-len 0) -1 MAX_SPEED)
     (check-feasible static-model acc-ns target-distance target-speed)
     MAX_SPEED
     :else
     (binary-search-speed static-model dec-ns target-distance target-speed))
    )
  )
