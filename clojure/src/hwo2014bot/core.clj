(ns hwo2014bot.core
  (:require [clojure.data.json :as json])
  (:require [hwo2014bot.model :refer :all]
            [hwo2014bot.speed-advisor :refer :all]
;;            [korma.core :refer [insert values update where set-fields]]
;;            [hwo2014bot.database :refer :all]
            )
  (:use [aleph.tcp :only [tcp-client]]
        [lamina.core :only [enqueue wait-for-result wait-for-message]]
        [gloss.core :only [string]]
        )
  (:gen-class))

(defn- json->clj [string]
  (json/read-str string :key-fn keyword))

(defn send-message [channel message]
  (enqueue channel (json/write-str message)))

(defn read-message [channel]
  (json->clj
    (try
      (wait-for-message channel)
      (catch Exception e
        (println (str "ERROR: " (.getMessage e)))
        (System/exit 1)))))

(defn connect-client-channel [host port]
  (wait-for-result
   (tcp-client {:host host,
                :port port,
                :frame (string :utf-8 :delimiters ["\n"])})))

(def race-id (atom 0))

(def init-history (atom []))

(def state (atom {:speed 0
                  :past-pcs-distance 0
                  :distance 0
                  :in-pc-dist 0
                  :pc 0
                  :lane-to 0
                  :lane-from 0
                  :angle 0
                  :angle-speed 0
                  :angle-acc 0
                  :radius 0
                  }))

(def track-data (atom nil))

(def policy (atom {:init 9}))

(def turbo (atom {:turboDurationTicks 0
                  :turboFactor 3.0}))

(def slip-angles (atom []))

(defn- fit-acc-model [speeds]
  (let [spds (remove zero? speeds)
        acc (map - (rest spds) spds)
        a (map -  (rest acc) acc)
        c1 (map / a acc)
        c2 (map - acc (map * c1 spds))]
    {:c1 (/ (apply + c1) (count c1))
     :c2 (/ (apply + c2) (count c2))}))

(defn control-speed [curr-speed target-speed]
  (let [delta (- target-speed curr-speed)
        params(:params @track-data)
        c2 (:c2 params)
        c1 (:c1 params)]
    (if (pos? delta)
      (if (> delta (+ c2 (* c1 target-speed)))
        (do ;; (println "accelerate" target-speed curr-speed)
            1.0)
        (let [throttle (/ (- delta (* c1 curr-speed))c2)]
          (do (println "maintain" target-speed curr-speed throttle)
              throttle)))
      (if (<  delta (+ c2 (* c1 target-speed)))
        (do ;;(println "decelerate" target-speed curr-speed)
            0.0)
        (let [throttle (/ (- delta (* c1 curr-speed))c2)]
          (do (println "maintain" target-speed curr-speed throttle)
              throttle))
        ))
    ))


(defn compute-policy [data]
  (let [path-model (:path-model @track-data)
        curr-lap (get-in data [:piecePosition :lap])
        p-state (carPos-to-path-state data)
        lane-policy (shortest-path path-model
                                   p-state
                                   (inc curr-lap))]

    {:lap curr-lap
     :pol lane-policy
     :limit (compute-speed-limits (:static-model @track-data)
                                  path-model
                                  lane-policy
                                  p-state
                                  (:distance @state)
                                  )})
  )

(defn update-state [msg]
  (let [data (first (:data msg))
        curr-pi (get-in data [:piecePosition :pieceIndex])
        prev-pi (:pc @state)
        curr-lap (get-in data [:piecePosition :lap])
        curr-past-pcs-dist
        (if (= prev-pi curr-pi)
          ;;; still in the same piece
          (:past-pcs-distance @state)
          ;; entered a new piece
          (let [prev-pc (get-in @track-data [:path-model :pieces prev-pi])
                _ (println "entering new piece " curr-pi)
                ]
            (+ (:past-pcs-distance @state)
               (distance prev-pc
                         (:lane-from @state)
                         (:lane-to @state)
                         ))))

        curr-distance (+ curr-past-pcs-dist
                         (get-in data
                                 [:piecePosition
                                  :inPieceDistance]))

        speed (- curr-distance (:distance @state))
        angle (:angle data)
        lane-from (get-in data [:piecePosition
                                :lane
                                :startLaneIndex])
        lane-to (get-in data [:piecePosition
                              :lane
                              :endLaneIndex])
        radius (let [r (one-over-radius
                        (get-in @track-data [:path-model :pieces curr-pi])
                        lane-from
                        lane-to)]
                 (if (zero? r)
                   0
                   (/ 1 r)))
        curr-angle-speed (- (:angle data) (:angle @state))
        curr-angle-acc (- curr-angle-speed (:angle-speed @state))
        ;;_ (println curr-distance speed angle (get-in data [:piecePosition :pieceIndex]))
        ]
    (do
      (swap!
       state
       (fn [s]
         (-> s
             (conj {:distance curr-distance
                    :past-pcs-distance curr-past-pcs-dist
                    :speed speed
                    :angle angle
                    :radius radius
                    :angle-speed curr-angle-speed
                    :angle-acc curr-angle-acc})
             (#(if (= curr-pi prev-pi)
                 %
                 (conj % {:lane-from lane-from
                          :lane-to lane-to
                          :pc curr-pi})))
             )
         ))
      (comment (insert race_data (values (-> @state
                                     (conj {:race @race-id})
                                     (dissoc :in-pc-dist)
                                     (dissoc :past-pcs-distance)
                                     (dissoc :pc)
                                     (assoc :piece (:pc @state))
                                     ))))
      (if (and (> curr-lap (get @policy :lap -1))
               (contains? @track-data :static-model))
        (do (swap!
             policy
             (fn [p]
               (conj p (compute-policy data))))
            (println @policy)
            )))
    ))

(defmulti handle-msg :msgType)

(defmethod handle-msg "gameInit" [msg]
  (do
    (let [track (get-in msg [:data :race :track])]
      (swap! track-data
             (fn [_] (assoc track
                       :path-model (make-path-model track))))
      (comment (swap! race-id (fn [id]
                        (second (first (insert races
                                               (values
                                                {:c1 0 :c2 0})))))))
      )
    {:msgType "ping" :data "ping"}))


;; (+ 5 (get-in msg [:data 0 :piecePosition :lap]))

(defn speed-advisor-controller [data state]
  (let [next-pc (mod (inc (get-in state [:pc]))
                     (count (get-in @track-data [:pieces])))
        pol-act ((:pol @policy) next-pc)
        target-speed (speed-advisor (:static-model @track-data) (:limit @policy)
                                    (:speed state) (:distance state)
                                    (:angle data)
                                    (:turboDurationTicks @turbo)
                                    (:turboFactor @turbo))]
    (if (not= 0 pol-act)
      (do
        (println "switching lane next")
        (swap! policy (fn [p] (assoc-in p [:pol next-pc] 0)))
        {:msgType "switchLane"
         :data (case pol-act
                 -1 "Left"
                 1 "Right")} )
      (if (and (= target-speed -1) (>= (:turboDurationTicks @turbo) 1))
        (do
          (swap! turbo (fn [t] (assoc t :turboDurationTicks 0)))
          (println "i'm on turbo!!")
          {:msgType "turbo" :data "Pow pow pow pow pow"}
          )
        {:msgType "throttle" :data (control-speed (:speed state) target-speed)})
      )
    ))


(defmethod handle-msg "turboAvailable" [msg]
  (do
    (println "got turbo")
    (swap! turbo (fn [_] (:data msg)))
    {:msgType "ping" :data "ping"})
)

(defmethod handle-msg "carPositions" [msg]
  (let [data (first (:data msg))
        _ (update-state msg)]
    (cond
     (> (:init @policy) 0)
     (do
       ;; collect data to fit parameter c1 and c2
       (swap! init-history (fn [h] (conj h @state)))
       (swap! policy (fn [p] (update-in p [:init] dec)))
       (println @state)
       [{:msgType "throttle" :data 1.0}])
     (= (:init @policy) 0)
     ;; fit parameter c1 and c2
     (do
       (let [params (fit-acc-model (map :speed @init-history))
             _ (println params)]
         (swap! track-data
                (fn [td]
                  (conj td {:params params
                            :static-model
                            (make-static-model @track-data
                                               (:c1 params)
                                               (:c2 params))})))
         (swap! policy (fn [p] (conj p (compute-policy data))))
         (comment (update races
                  (set-fields {:c1 (:c1 params)
                               :c2 (:c2 params)})
                  (where {:id [= @race-id]})))
         )
       (swap! policy (fn [p] (update-in p [:init] dec)))
       [{:msgType "throttle" :data 1.0}]
       )
     :else
     (assoc (speed-advisor-controller data @state)
           :gameTick (:gameTick msg))
     ))
  )

(defmethod handle-msg "gameEnd" [msg]
  (do
    (spit "slip_angles.dat" @slip-angles)
    {:msgType "ping" :data "ping"}))

(defmethod handle-msg :default [msg]
  {:msgType "ping" :data "ping"})

(defn log-msg [msg]
  (case (:msgType msg)
    "join" (println "Joined")
    "gameStart" (println "Race started")
    "crash" (println "Someone crashed")
    "gameInit" (println "Game Init")
    "gameEnd" (println "Race ended")
    "lapFinished" (println (get-in msg [:data :lapTime]))
    "error" (println (str "ERROR: " (:data msg)))
    :noop))

(defn game-loop [channel]
  (let [msg (read-message channel)]
    (do (log-msg msg)
        (send-message channel (handle-msg msg))
        (recur channel))))

(defn -main[& [host port botname botkey]]
  (let [channel (connect-client-channel host (Integer/parseInt port))]
    (send-message channel {:msgType "join" :data {:name botname :key botkey}})
    (game-loop channel)))

(comment
  (dorun (let [channel (connect-client-channel "testserver.helloworldopen.com" 8091)]
        (send-message channel {:msgType "join" :data {:name "motion" :key "Cvl+PFH6JWzmIg"}})
        (game-loop channel))))
