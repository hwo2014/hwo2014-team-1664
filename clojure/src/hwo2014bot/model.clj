(ns hwo2014bot.model)

(defprotocol Piece
  (distance [this from-lane to-lane])
  (one-over-radius [this from-lane to-lane])
  (switchable [this])
  )

(defrecord StraightPiece
    [length
     switch
     lane-offsets]
  Piece
  (distance [this from-lane to-lane]
    (if (= from-lane to-lane)
      length
      (let [lane-dist (Math/abs (- (lane-offsets from-lane)
                                   (lane-offsets to-lane)))]
        (Math/sqrt (+ (Math/pow length 2) (Math/pow lane-dist 2))))
      ))
  (one-over-radius [this from to]
    0)
  (switchable [this]
    switch))

(defrecord BendPiece
    [radius
     angle
     switch
     lane-offsets]
  Piece
  (distance [this from-lane to-lane]
    (Math/abs
     (let [offset-fn (if (pos? angle) - +)]
       (if (= from-lane to-lane)
         (* (offset-fn radius (lane-offsets from-lane)) Math/PI angle 1/180)
         (* Math/PI angle 1/180 radius 1.03183))))
    )
  (one-over-radius [this from-lane to-lane]
    (if (= from-lane to-lane)
      (/ 1 (+ radius (lane-offsets from-lane)))
      (/ 1 radius)))
  (switchable [this]
    switch)
  )

(defn make-piece [piece-data lane-offsets]
  (if (contains? piece-data :length)
    (->StraightPiece
     (:length piece-data)
     (get piece-data :switch false)
     lane-offsets)
    (->BendPiece
     (:radius piece-data)
     (:angle piece-data)
     (get piece-data :switch false)
     lane-offsets)
    ))

(defn make-pieces [track-data]
  (let [pieces (:pieces track-data)
        lane-offsets (mapv
                      :distanceFromCenter
                      (sort-by :index (:lanes track-data)))
        ]
    (mapv #(make-piece % lane-offsets) pieces)))

(defprotocol RaceModel
  (next-state [this curr-state action])
  )

(def init-static-state {:distance 0
                        :speed 0
                        :angle 0})

(defrecord RaceModelStatic
    [pieces
     c1
     c2
     sa1
     sa2]
  RaceModel
  (next-state [this state action]
    (let [acc (+ (* c2 action) (* c1 (:speed state)))]
      {:distance (+ (:distance state) (:speed state))
       :speed (+ acc (:speed state))
       :angle 0
       })
    )
  )

(defrecord RacePathModel
    [pieces
     num-lanes]
  RaceModel
  (next-state [this curr-state action]
    (let [curr-piece (pieces (:pi curr-state))
          curr-lane (:lane curr-state)
          curr-distance (:distance curr-state)
          ]
      (if (or (< action 0)
              (> action (dec num-lanes))
              (and (not (switchable curr-piece))
                   (not= action (:lane curr-state) )))
        (do (println "invalid action" action " at lane " curr-piece "lane"
                     curr-lane)
            nil
            )
        (-> curr-state
            (assoc :pi
              (mod (inc (:pi curr-state)) (count pieces)))
            (assoc :lap
              (if (>= (inc (:pi curr-state)) (count pieces))
                (inc (:lap curr-state))
                (:lap curr-state)))
            (assoc :distance
              (+ (distance curr-piece curr-lane action)
                 curr-distance))
            (assoc :lane action))
        )
      )))

(defn make-static-model [track-data c1 c2]
  (->RaceModelStatic (make-pieces track-data)
                     c1 c2 0.91 0.83))

(defn make-path-model [track-data]
  (let [pieces (make-pieces track-data)]
    (->RacePathModel pieces
                     (count (:lane-offsets (first pieces))))))

(defn carPos-to-path-state [carPos]
  (let [piece-pos (:piecePosition carPos)
        end-lane (get-in piece-pos [:lane :endLaneIndex])
        pi (:pieceIndex piece-pos)
        lap (:lap piece-pos)]
    {:lap lap
     :pi pi
     :lane end-lane
     :distance 0}))

(comment
  ;; full model state
  {:speed (rand 12.0)
  :in-pc-dist (rand (distance (pieces pc) lane-to lane-from))
  :pc pc
  :radius 0
  :lane-to lane-to
  :lane-from lane-from
  :angle 0
  :angle-acc 0
  :angle-speed 0})

(defrecord RaceModelFull
    [pieces
     lane-offsets
     c1
     c2
     sa1
     sa2
     cum-dist]
  RaceModel
  ;;; quick hack to use macro actions
  (next-state [this state action]
    (let [acc (+ (* c1 (:throttle action)) (* c2 (:speed state)))
          inter-speeds (for [i (range 3)]
                       (+ (* i acc) (:speed state)))
          new-distance (+ (:in-pc-dist state) (apply + inter-speeds))
          new-speed (last inter-speeds)
          pc-distance (distance (pieces (:pc state))
                                (:lane-from state)
                                (:lane-to state)
                                )
          ]
      (if (and (>= (:speed state) 6)
               (> (one-over-radius (pieces (:pc state)) 0 0) 0))
        ;; simulate crash
        (conj state {:speed 0 :crash true})
        (->
         state
         (dissoc :crash)
         (#(if (> new-distance pc-distance)
             (conj % {:in-pc-dist (- new-distance pc-distance)
                      :lane-to (+ (:lane-delta action) (:lane-to state))
                      :lane-from (:lane-to state)
                      :pc (mod (inc (:pc state)) (count pieces))})
             (conj % {:in-pc-dist new-distance})))
         (assoc :speed new-speed)
         ))
      )
    )
  )

(defn make-full-model [track-data]
  (let [pieces (make-pieces track-data)
        ]
    (->RaceModelFull pieces
                     (:lane-offsets (first pieces))
                     0.2 -0.02 0.91 0.83
                     (loop [pp pieces
                            d 0
                            cd []]
                       (if (empty? pp)
                         (conj cd d)
                         (recur (rest pp)
                                (+ d (distance (first pp) 0 0))
                                (conj cd d)))))))
