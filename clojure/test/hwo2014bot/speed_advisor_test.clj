(ns hwo2014bot.speed-advisor-test
  (:require [clojure.test :refer :all]
            [hwo2014bot.model :refer :all]
            [hwo2014bot.speed-advisor :refer :all])
  )

(def finland-track (read-string (slurp "finland.dat")))

(def path-model (make-path-model finland-track))

(def static-model (make-static-model finland-track))

(deftest test-path-search
  (let [start-state {:lap 1 :pi 0 :lane 1 :distance 0}]
    (is (= 0 (shortest-path path-model start-state 2))))
  )


(deftest test-path-length
  (let [start-state {:lap 1 :pi 0 :lane 1 :distance 0}]
    (is (= 0
           (loop [s start-state]
             (if (>= (:lap s) 2)
               (:distance s)
               (let [pc ((:pieces path-model) (:pi s))
                     ;act (if (and (= (:lane s) 1)
                     ;             (switchable pc))
                     ;      0
                     ;      (:lane s))
                     act 1
                     _ (println act s)
                     ]
                 (recur (next-state path-model s act)))))))))


(deftest test-race-state-model
  )

;; TODO: fill up this test
(deftest test-speed-limit
  (let [start-p-state {:lap 1 :pi 0 :lane 1 :distance 0}
        policy (shortest-path path-model start-p-state 2)
        ]
    (is (nil? (compute-speed-limits static-model path-model policy start-p-state)))
    ))


(deftest test-check-feasible
  (let [curr-state {:speed 10 :distance 100 :angle 0}
        target-distance 300
        target-speed 6
        ]
    (is (= true (check-feasible static-model curr-state target-distance target-speed) ))
    (is (= false (check-feasible static-model (assoc curr-state
                                                :distance 200) target-distance target-speed) ))
    )
  )

(deftest test-binary-search
  (let [curr-state {:speed 10 :distance 200 :angle 0}
        target-distance 300
        target-speed 6
        ]
    (is (= 0 (binary-search-speed static-model curr-state target-distance target-speed)))
    ))
