(ns hwo2014bot.astar-test
  (:require [hwo2014bot.model :refer :all]
            [hwo2014bot.astar :refer :all]
            [clojure.test :refer :all]))


(def finland-track (read-string (slurp "finland.dat")))

(def full-model (make-full-model finland-track))

(def ts {:speed 9.6
            :in-pc-dist 98.4
            :pc 10
            :lane-to 0
            :lane-from 0
            :angle 0
            :angle-speed 0
            :angle-acc 0})

(deftest test-discretize
  (is (= {:speed 9.0
          :in-pc-dist 100.0
          :pc 10
          :lane-to 0
          :lane-from 0
          :angle 0
          :angle-speed 0
          :angle-acc 0}
         (discretize-state ts))))

(deftest test-neighbour-nodes
  (let [ts1 (conj ts {:pc 2 :in-pc-dist 95})
        ts2 (conj ts {:pc 2 :in-pc-dist 10})
        ]
    (is (= 6 (count (neighbour-nodes full-model ts1))))
    (is (= 3 (neighbour-nodes full-model ts)))
    ))

(deftest test-astar
  (let [start (conj ts {:speed 0.0 :in-pc-dist 0 :pc 0})]
    (is (nil? (time (astar full-model start (count (:pieces full-model))))))))
