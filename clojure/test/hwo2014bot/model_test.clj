(ns hwo2014bot.model-test
  (:require [clojure.test :refer :all]
            [hwo2014bot.model :refer :all])
  )

(def finland-track (read-string (slurp "finland.dat")))

(def path-model (make-path-model finland-track))

(def static-model (make-static-model finland-track))

(def full-model (make-full-model finland-track))

(deftest test-path-model-data
  (let [pieces (:pieces path-model)
        straight-pc (first pieces)
        bend-pc (pieces 4)
        switch-pc (pieces 3)
        pc-24 (pieces 24)]
    (is (= (distance pc-24 0 0) 74.61282552275759))
    (is (= 1/190 (one-over-radius pc-24 0 0)))
    (is (= (:lane-offsets pc-24) [-10 10]))
    (is (= (distance straight-pc 0 0)
           100.0))
    (is (= (distance switch-pc 0 1)
           101.9803902718557) )
    (is (switchable switch-pc))
    (is (not (switchable straight-pc)))
    (is (distance bend-pc 0 0)
        70.685834706)
    (is (= (next-state path-model
                       {:lap 0 :lane 0 :distance 0 :pi (dec (count pieces))}
                       0)
           { :pi 0 :lap 1 :lane 0 :distance 90.0}))
    )
  )

(deftest test-full-model
  (let [ts {:speed 10
            :in-pc-dist 10
            :pc 10
            :lane-to 0
            :lane-from 0
            :angle 0
            :angle-acc 0
            :angle-speed 0}
        ts-switch (conj ts {:pc 3 :in-pc-dist 95})
        ]
    (is (= {:in-pc-dist 20, :speed 10.0, :lane-from 0, :angle 0, :lane-to 0,
            :pc 10, :angle-acc 0
            :angle-speed 0}
           (next-state full-model ts {:throttle 1.0 :lane-delta 0}))
        )
    (is (= {:in-pc-dist 20, :speed 9.8, :lane-from 0, :angle 0, :lane-to 0
            , :pc 10, :angle-acc 0 :angle-speed 0}
           (next-state full-model ts {:throttle 0.0 :lane-delta 0})))
    (is (= {:in-pc-dist 5.0, :speed 10.0, :lane-from 0, :angle 0, :lane-to 1
            , :pc 4 :angle-acc 0 :angle-speed 0}
           (next-state full-model ts-switch {:throttle 1.0 :lane-delta 1})
           ))
    ))
