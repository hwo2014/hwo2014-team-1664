(ns hwo2014bot.rrt-test
  (:require [hwo2014bot.rrt :refer :all]))

(def finland-track (read-string (slurp "finland.dat")))

(def static-model (make-static-model finland-track))
